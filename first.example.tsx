// Package
import React from 'react'
import { Row, Col, Card, Tab } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

// Components
import { ApplicationPaths } from 'components/api-authorization/ApiAuthorizationConstants'

// Models
import { IUser } from '../../models'

interface IProps {
  user: IUser
  rtlLayout: boolean
  onChangeNavFixedLayout: () => void
}

class Settings extends React.Component<IProps> {
  render = () => {
    const { user, rtlLayout, onChangeNavFixedLayout } = this.props

    const logoutPath = {
      pathname: `${ApplicationPaths.LogOut}`,
      state: { local: true },
    }

    return (
      <Row className="btn-page">
        <Col sm={12}>
          <Card>
            <Card.Body>
              <Tab.Container>
                <Row>
                  <Col sm={12}>
                    <h2> ברוך הבא {user.preferred_username}!</h2>
                    <h2>התפקיד שלך {user.role}!</h2>
                    <div className="form-group mb-0">
                      <div className="switch switch-primary d-inline m-r-10">
                        <input
                          type="checkbox"
                          id="RTL"
                          checked={rtlLayout}
                          onChange={onChangeNavFixedLayout}
                        />
                        <label htmlFor="RTL" className="cr" />
                      </div>
                      <label>RTL</label>
                    </div>
                    <br />
                    <NavLink to={logoutPath}>
                      <button className="btn btn-warning">להתנתק</button>
                    </NavLink>
                  </Col>
                </Row>
              </Tab.Container>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = (state: IProps) => {
  return {
    rtlLayout: state.rtlLayout,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    onChangeNavFixedLayout: () => dispatch({ type: 'RTL_LAYOUT' }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
