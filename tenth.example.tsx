import React, { useState, useEffect, useRef } from 'react'
// @ts-ignore
import { useStore } from 'stores'
import { useObserver } from 'mobx-react'
 
const Image = () => {
  const { builder } = useStore()
  const containerRef = useRef<HTMLDivElement>(null)
 
  const [initialPosition, handleInitialPos] = useState({ x: 0, y: 0 })
  const [currentPos, handleCurrentPos] = useState({ x: 0, y: 0 })
  const [offsetPos, handleOffsetPos] = useState({ x: 0, y: 0 })
 
  const setPosition = (x: any, y: any, elem: any) => {
    if (elem === null) {
      if (containerRef && containerRef.current) {
        containerRef.current.style.backgroundPositionX = x + 'px'
        containerRef.current.style.backgroundPositionY = y + 'px'
      }
    } else {
      elem.target.style.backgroundPositionX = x + 'px'
      elem.target.style.backgroundPositionY = y + 'px'
    }
  }
 
  useEffect(() => {
    setPosition(builder.imageBlock.styles.x, builder.imageBlock.styles.y, null)
  }, [])
 
  const [active, handleActive] = useState(false)
  const onDragStart = async (e: any) => {
    let { x, y } = initialPosition
    if (e.type === 'touchstart') {
      x = e.touches[0].clientX - offsetPos.x
      y = e.touches[0].clientY - offsetPos.y
    } else {
      x = e.clientX - offsetPos.x
      y = e.clientY - offsetPos.y
    }
 
    await handleInitialPos({ x, y })
    await handleActive(true)
  }
 
  const onDragEnd = () => {
    handleInitialPos({ x: currentPos.x, y: currentPos.y })
    builder.handleChangePosition({ x: currentPos.x, y: currentPos.y })
    handleActive(false)
  }
 
  const onDrag = (e: any) => {
    if (active) {
      e.preventDefault()
      let { x, y } = currentPos
      if (e.type === 'touchmove') {
        x = e.touches[0].clientX - initialPosition.x
        y = e.touches[0].clientY - initialPosition.y
      } else {
        x = e.clientX - initialPosition.x
        y = e.clientY - initialPosition.y
      }
 
      handleOffsetPos({ x, y })
      handleCurrentPos({ x, y })
      setPosition(x, y, e)
    }
  }
 
  return useObserver(() => (
    <div
      ref={containerRef}
      style={{
        backgroundImage: `url(${builder.imageBlock.imageSrc})`,
        backgroundSize: `${builder.imageBlock.styles.scale}%`,
        backgroundRepeat: 'no-repeat',
        borderRadius: `${builder.imageBlock.styles.borderRadius}%`,
        width: builder.imageBlock.styles.width,
        height: builder.imageBlock.styles.height,
        border: '2px dashed #545454',
      }}
      onMouseDown={onDragStart}
      onTouchStart={onDragStart}
      onMouseUp={onDragEnd}
      onTouchEnd={onDragEnd}
      onMouseMove={onDrag}
      onTouchMove={onDrag}
    ></div>
  ))
}
 
export default Image
