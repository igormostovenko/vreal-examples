import React, { useEffect, useState } from 'react'
import styles from './styles.module.scss'
import { useStore } from 'stores'
import Header from './Header'
import ControlHeader from './ControlHeader'
import ListItem from './ListItem'
import { useObserver } from 'mobx-react'
import Loader from 'components/Loader'
import InfiniteScroll from 'react-infinite-scroll-component'
import { Button } from 'r-components'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import EmptyList from 'components/EmptyList'
 
const PositionList: React.FC = () => {
  const { positions } = useStore()
  const [editable, handleEditable] = useState<number[]>([])
 
  const handleItemClick = (index: number) => {
    let activeIndex = _.cloneDeep(editable)
    let findIndex = activeIndex.findIndex(idx => idx === index)
    if (findIndex > -1) {
      activeIndex.splice(findIndex, 1)
    } else {
      activeIndex.push(index)
    }
    handleEditable(activeIndex)
  }
 
 
  useEffect(() => {
    positions.getAll()
  }, [])
 
  const next = async () => {
    if (positions.pagination.limit === 50) return
    await positions.handlePagination('limit', positions.pagination.limit + 10)
    await positions.getAll(true)
  }
 
  const handleLoadMore = async () => {
    await positions.handlePagination('skip', positions.pagination.skip + 50)
    await positions.handlePagination('limit', 20)
    window.scrollTo(0, 0)
    await positions.getAll(true)
  }
 
  return useObserver(() => (
    <>
      <Header />
      <ControlHeader />
      {positions.loading ? (
        <Loader />
      ) : positions.list.length ? (
        <>
          <InfiniteScroll
            next={next}
            dataLength={positions.list.length}
            hasMore={positions.totalRecords > positions.pagination.skip + positions.list.length}
            loader={positions.list.length < 50 && <Loader />}
          >
            {positions.list.map((item, index) => (
              <ListItem item={item} key={index} onClick={() => handleItemClick(index)} active={editable.findIndex(idx => idx === index) > -1 || !item.published} />
            ))}
          </InfiniteScroll>
          {positions.list.length === 50 && (
            <div style={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
              <Button
                containerClassName={styles.button}
                onClick={() => handleLoadMore()}
                transparent
                color="#0090b0"
              >
                Load More
              </Button>
            </div>
          )}
        </>
      ) : (
            <EmptyList>
              <p>
                There’s no positions to list at the moment,<br /> but you can <Link to="/create">create</Link> one.
              </p>
            </EmptyList>
          )}
    </>
  ))
}
 
export default PositionList
