import React, { useEffect, useState } from 'react'
import styles from './styles.module.scss'
 
import { Table, Header, Row, Cell, HeaderCell } from 'components/Table'
import moment from 'moment'
import { Icon, Input, Switch } from 'r-components'
import history from 'utils/history'
// @ts-ignore
import InfiniteScroll from 'react-infinite-scroller';
import { useStore } from 'stores'
import Loader from 'components/Loader'
import { useObserver } from 'mobx-react'
import { ISubmissionKeys } from 'types/Submission'
import { ISort } from 'types/Sort'
import { toJS } from 'mobx'
import _ from 'lodash'
 
 
const primaryBlue = '#5bb8cf'
const gray = '#d6d6d6'
const lightGray = '#c7c7c7'
 
const timeFormat = 'MM/DD/YY'
 
interface IControlIconProps {
  name: string
  active: boolean
}
 
const ControlIcon: React.FC<IControlIconProps> = (props) => {
  let name = props.name
  if (props.active) name = name + '-solid'
  return (
    <Icon
      name={name}
      style={{
        cursor: 'pointer',
        color: props.active ? primaryBlue : gray,
        fontSize: 20,
      }}
    />
  )
}
 
const debouncer = _.debounce(f => f(), 1500, { leading: false })
const Dashboard = () => {
  const { submissions } = useStore()
  useEffect(() => {
    const gets = async () => {
      await submissions.getAll()
      // await submissions.getEmployerStatuses()
    }
    gets()
  }, [])
 
  const next = async () => {
    await submissions.handlePagination('limit', submissions.pagination.limit + 10)
    await submissions.getAll(true)
  }
 
  const handleItemClick = (id: string, viewed: boolean) => {
    history.push(`/${id}/submissions/${id}`)
    if (!viewed) submissions.patcthSubmission(id, 'viewed' as ISubmissionKeys, viewed)
  }
 
  const handleIconClick = async (
    id: string,
    field: ISubmissionKeys,
    value: boolean
  ) => {
    await submissions.patcthSubmission(id, field, value)
    await submissions.getAll()
  }
 
  const handleSort = async (value: ISort) => {
    let val = _.cloneDeep(toJS(value))
    if (val.value === 'asc') {
      val.value = 'desc'
    } else {
      val.value = 'asc'
    }
 
    await submissions.handleSort(val)
    await submissions.getAll()
  }
 
  const onChangeData = () => {
    debouncer(() => {
      const promise = async () => {
        await submissions.getAll()
      }
      promise()
    })
  }
 
  const handleEvaluationStatus = async (value: number) => {
    if (value === 2) {
      value = 1
    } else {
      value = 2
    }
    await submissions.handleEvaluationStatus(value)
    await submissions.getAll()
  }
 
  return useObserver(() => (
    <div className={styles.pageContainer}>
      <div className={styles.container}>
        <div className={styles.header}>
          <div>Dashboard</div>
        </div>
 
        <div className={styles.filter}>
          <div>
            <Input
              placeholder="Search by Name"
              icon={
                <Icon name="symbol-search" style={{ fontSize: 20, color: '#1f1f1f' }} />
              }
              value={submissions.query}
              onChangeValue={async (value: string) => {
                submissions.handleQuery(value)
                await onChangeData()
              }}
            />
          </div>
          <div className={styles.toggle}>
            <label>Display Inactive Candidates</label>
            <Switch value={submissions.evaluationStatusId === 2 ? false : true} onChangeValue={() => handleEvaluationStatus(submissions.evaluationStatusId)} />
          </div>
        </div>
 
        <div className={styles.tableContainer}>
          <Table>
            <Header>
              <Row>
                <HeaderCell width="90px" sortable sortFunction={handleSort} value={submissions.createdAt} >
                  Date
                </HeaderCell>
                <HeaderCell width="30px" align="center" sortable sortFunction={handleSort} value={submissions.favorite} />
                <HeaderCell width="30px" align="center" sortable />
                <HeaderCell sortable sortFunction={handleSort} value={submissions.name}>Name</HeaderCell>
                <HeaderCell align="center" width="120px">Contact</HeaderCell>
                <HeaderCell width="170px">Status</HeaderCell>
                {/* <HeaderCell width="130px">Core 4 Work Styles</HeaderCell> */}
              </Row>
            </Header>
            <InfiniteScroll
              pageStart={0}
              loadMore={next}
              hasMore={submissions.totalRecords > submissions.list.length}
              loader={
                <Row>
                  <Cell colspan={7}><Loader /></Cell>
                </Row>
              }
              useWindow={true}
              element={'tbody'}
            >
              {submissions.list.map((item, index) => (
                <Row bold={!item.viewed} inActive={item.evaluationStatus.name !== 'Matched'} key={index}>
                  <Cell>
                    {moment(item.createdAt).format(timeFormat)}
                  </Cell>
                  <Cell onClick={() => handleIconClick(item.id, 'favorite', item.favorite)} align="center">
                    <ControlIcon name="symbol-favourite" active={item.favorite} />
                  </Cell>
                  <Cell align="center">
                    <ControlIcon name="symbol-flagged" active={false} />
                  </Cell>
                  <Cell onClick={() => handleItemClick(item.id, item.viewed)}>
                    <div style={{ paddingLeft: 7 }}>
                      {`${item.candidate.firstName} ${item.candidate.lastName}`}
                    </div>
                  </Cell>
                  <Cell align="center">
                    <a href={`mailto:${item.candidate.email}`}>
                      <Icon style={{ fontSize: 24, color: '#5bb8cf' }} name="control-contact" />
                    </a>
                  </Cell>
                  <Cell>{item.employerStatus.name}</Cell>
                  {/* <Cell>
                    <DisplayWorkStyles styles={false} />
                  </Cell> */}
                </Row>
              ))}
            </InfiniteScroll>
          </Table>
        </div>
      </div>
    </div>
  ))
 
}
 
export default Dashboard