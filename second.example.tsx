import React from 'react'
import styles from './styles.module.scss'
import classnames from 'classnames'
import { Switch, Button, Input, Icon } from 'r-components'
import { useStore } from 'stores'
import history from 'utils/history'
import _ from 'lodash'
import { useObserver } from 'mobx-react'
import { notification } from 'antd'
import { debouncer } from 'utils/const'


const debounce = debouncer(1500)
const ControlHeader = () => {
  const { positions } = useStore()

  const handleChangeValue = () => {
    positions.toggleArchived()
    positions.getAll()
  }

  const onChangeData = () => {
    debounce(() => {
      const promise = async () => {
        if (positions.query.length >= 3) {
          await positions.getAll()
        } else if (!positions.query) {
          await positions.getAll()
        } else {
          notification.warn({ message: 'Minimum 3 characters required' })
        }
      }
      promise()
    })
  }

  return useObserver(() => (
    <div className={styles.controlHeader}>
      <div className={styles.container}>
        <Button containerClassName={styles.buttonContainer} onClick={() => history.push('/create')}>
          Create Position
        </Button>
        <div className={styles.inputContainer}>
          <Input
            placeholder="Position Title"
            value={positions.query}
            onChangeValue={async (value: string) => {
              positions.handleQuery(value)
              await onChangeData()
            }}
            icon={
              <Icon name="symbol-search" style={{ fontSize: 20, color: '#1f1f1f' }} />
            }
          />
        </div>
      </div>
      <div className={styles.control}>
        <label>Show Archived Positions</label>
        <Switch value={positions.archived} onChangeValue={handleChangeValue} />
      </div>
    </div>
  ))
}

export default ControlHeader
