import React, { Component, ReactElement } from 'react'
import { Switch as AndtSwitch, Typography } from 'antd'

import styles from './styles.module.scss'
import { SwitchChangeEventHandler } from 'antd/lib/switch'

interface IProps {
  className?: string
  label?: string
  checked?: boolean
  onChange?: SwitchChangeEventHandler
}

const { Text } = Typography

class Switch extends Component<IProps, {}> {
  render = (): ReactElement => {
    const { label, onChange, checked } = this.props

    return (
      <div className={styles.switch}>
        <AndtSwitch checked={checked} onChange={onChange} />
        {label && <Text className={styles.labelText}>{label}</Text>}
      </div>
    )
  }
}

export default Switch
