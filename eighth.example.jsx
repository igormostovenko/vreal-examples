import React, { Component, Fragment } from 'react'
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native'
import { connect } from 'react-redux'
import _ from 'lodash'
import Mailer from 'react-native-mail'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Header from './header'

import * as questionnaireResultsActions from '../../actions/questionnaireResults'
import * as questionnairesActions from '../../actions/questionnaires'
import * as attachmentsActions from '../../actions/attachments'
import * as pdfActions from '../../actions/pdf'

import styles from './styles'
import i18n from '../../i18n'

const categories = [
  { id: 1, name: i18n.t('questionnarie_titleFirst'), color: '#FFCD10' },
  { id: 2, name: i18n.t('questionnarie_titleSecond'), color: '#86BD24' },
  { id: 3, name: i18n.t('questionnarie_titleThird'), color: '#1774BF' },
  { id: 4, name: i18n.t('questionnarie_titleFour'), color: '#EE720F' },
  { id: 5, name: i18n.t('questionnarie_titleFive'), color: '#A80A77' },
]

class Overview extends Component {
  state = {
    categoryErrors: {},
    loading: false,
  }

  categoriesScore = {}
  categoriesAnswers = {}

  componentWillMount = () => {
    this.props.navigator.toggleNavBar({
      to: 'hidden',
      animated: false,
    })
    this.props.navigator.setStyle({
      statusBarTextColorScheme: 'dark',
      statusBarColor: '#fff',
      disabledBackGesture: true,
    })
    this.validateQuestionnaire(false)
  }

  onPressEnd = async () => {
    this.setState({ loading: true })

    try {
      this.validateQuestionnaire()

      let questionnaireResults = await questionnaireResultsActions.send()

      for (let questionnaireResult of questionnaireResults.questionResults) {
        if (
          !this.props.questionnaireResults[questionnaireResult.questionId].photo
        )
          continue

        if (questionnaireResult.id) {
          await attachmentsActions.upload(
            questionnaireResult.id,
            this.props.questionnaireResults[questionnaireResult.questionId]
              .photo
          )
        }
      }

      this.props.navigator.popToRoot({
        screen: 'home',
        animationType: 'fade',
        overrideBackPress: true,
      })

      Alert.alert('Audit', i18n.t('overview_result_sent_server'))

      questionnairesActions.unset()
      questionnaireResultsActions.unset()
    } catch (e) {
      Alert.alert('Error', e.message)
      this.setState({ loading: false })
    }
  }

  onPressCategory = index => {
    this.props.navigator.pop()
    this.props.backTo(index)
  }

  renderCategory = (category, index) => {
    let totalScore = 0
    let userScore = 0
    let questions = this.props.questionnaire.questions.filter(
      question => question.categoryId === category.id
    )
    let answers = []
    let results = []
    let hasError = this.state.categoryErrors[category.id] !== undefined

    for (let question of questions) {
      totalScore += Math.max.apply(
        Math,
        question.answers.map(answer => answer.score)
      )
      results = results.concat(
        _.values(this.props.questionnaireResults).filter(
          result => result.questionId === question.id
        )
      )
      answers = answers.concat(question.answers)
    }

    for (let result of results) {
      let answer = answers.find(answer => answer.id === result.answerId)
      userScore += answer.score
    }

    this.categoriesScore[category.id] = { userScore, totalScore }

    return (
      <TouchableOpacity
        key={`category-${index}`}
        onPress={() => this.onPressCategory(index)}
      >
        <View style={[styles.result, { backgroundColor: category.color }]}>
          {hasError && (
            <Icon
              name="exclamation-circle"
              size={25}
              color="#fff"
              style={styles.resultError}
            />
          )}
          <Text style={styles.resultTitle}>{category.name}</Text>
          <Text style={styles.resultScore}>
            {userScore} / {totalScore}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  onSendEmail = async scorePercent => {
    const location = this.props.locations.find(
      loc => loc.id === this.props.room.locationId
    )

    try {
      this.validateQuestionnaire()

      let pathToPdf = await pdfActions.generate(
        categories,
        this.categoriesScore,
        scorePercent,
        this.props.questionnaire.questions,
        this.props.questionnaireResults,
        this.props.room.name,
        location.name
      )

      Mailer.mail(
        {
          subject: i18n.t('overview_email_subject'),
          body: 'See PDF attachment',
          isHTML: false,
          attachment: {
            path: pathToPdf.filePath,
            type: 'pdf',
            name: 'audit.pdf',
          },
        },
        (error, event) => {
          if (event === 'sent') {
            Alert.alert('Audit', i18n.t('overview_result_sent_email'))
            return
          }
        }
      )
    } catch (e) {
      alert(e.message || e)
    }
  }

  validateQuestionnaire = (throwError = true) => {
    let categoryErrors = {}

    for (let question of this.props.questionnaire.questions) {
      let result = this.props.questionnaireResults[question.id]
      if (!result) {
        categoryErrors[question.categoryId] = true
        continue
      }

      let answer = question.answers.find(
        answer => answer.id === result.answerId
      )
      if (
        answer.score < 4 &&
        result.photo === null &&
        result.comment === null
      ) {
        categoryErrors[question.categoryId] = true
      }
    }

    this.setState({ categoryErrors })
    if (throwError && Object.keys(categoryErrors).length > 0) {
      throw new Error(i18n.t('overview_error'))
    }
  }

  render = () => {
    if (!this.props.questionnaire.id) return null

    let totalScore = 0
    let userScore = 0
    let answers = []

    for (let question of this.props.questionnaire.questions) {
      totalScore += Math.max.apply(
        Math,
        question.answers.map(answer => answer.score)
      )
      answers = answers.concat(question.answers)
    }

    for (let result of _.values(this.props.questionnaireResults)) {
      let answer = answers.find(answer => answer.id === result.answerId)
      userScore += answer.score
    }

    let scorePercent = Math.floor((userScore * 100) / totalScore)

    return (
      <View style={styles.container}>
        <Header />
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.wrapper}
        >
          {categories.map(this.renderCategory)}

          <View style={styles.divider} />
          <View style={styles.scoreView}>
            <Text style={styles.scoreTitle}>Score</Text>
            <Text style={styles.scorePercent}>{scorePercent}%</Text>
          </View>

          {this.state.loading ? (
            <View style={{ justifyContent: 'center', flex: 1 }}>
              <ActivityIndicator />
            </View>
          ) : (
            <View style={styles.btns}>
              <TouchableOpacity
                onPress={() => this.onSendEmail(scorePercent)}
                style={[styles.btn, styles.btnGray]}
                activeOpacity={0.8}
              >
                <Text style={styles.btnText}>
                  {i18n.t('overview_sendEmail')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this.onPressEnd}
                style={styles.btn}
                activeOpacity={0.8}
              >
                <Text style={styles.btnText}>
                  {i18n.t('overview_sendAudit')}
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </ScrollView>
      </View>
    )
  }
}

export default connect(state => ({
  questionnaire: state.questionnaire,
  questionnaireResults: state.questionnaireResults,
  room: state.room,
  locations: state.locations,
}))(Overview)
