// Package
import React, { Suspense } from 'react'
import { connect } from 'react-redux'
import { Switch, Route } from 'react-router'
import windowSize, { WindowSizeProps } from 'react-window-size'

// Components
import Navigation from '../../components/Navigation'
import NavBar from '../../components/NavBar'
import Loader from '../../components/Loader'
import Breadcrumb from '../../components/Breadcrumb'
import Assignment from '../Assignment'
import TasksList from '../TasksList'
import AddTask from '../AddTask'
import Settings from '../Settings'

// Models
import { IUser } from 'models/user'

interface IProps {
  user: IUser
  collapseMenu: boolean
  onToggleNavigation: () => void
}

class Dashboard extends React.Component<IProps & WindowSizeProps> {
  mobileOutClickHandler = () => {
    const { collapseMenu, windowWidth, onToggleNavigation } = this.props

    if (windowWidth < 992 && collapseMenu) {
      onToggleNavigation()
    }
  }

  render = () => {
    const { users } = this.props

    let role = user.role === 'Admin'
    return (
      <>
        <Navigation />
        <NavBar />
        <div
          className="pcoded-main-container"
          onClick={this.mobileOutClickHandler}
        >
          <div className={'pcoded-wrapper'}>
            <div className="pcoded-content">
              <div className="pcoded-inner-content">
                <Breadcrumb />
                <div className="main-body">
                  <div className="page-wrapper">
                    <Suspense fallback={<Loader color={'#fff'} />}>
                      <Switch>
                        {/*<Route*/}
                        {/*  path="/app/dashboard"*/}
                        {/*  component={TestComponent}*/}
                        {/*/>*/}
                        <Route path="/app/tasks" component={TasksList} />
                        <Route path="/app/add-task" component={AddTask} />
                        {/*<Route path="/app/reports" component={TestComponent} />*/}
                        <Route path="/app/settings" component={Settings} />
                        {role ? (
                          <>
                            <Route path="/app/courses" component={Assignment} />
                            {/*<Route*/}
                            {/*  path="/app/users"*/}
                            {/*  component={TestComponent}*/}
                            {/*/>*/}
                          </>
                        ) : (
                          <>
                            {/*// <Route*/}
                            {/*//   path="/app/tasks_revision"*/}
                            {/*//   component={TestComponent}*/}
                            {/*// />*/}
                          </>
                        )}
                      </Switch>
                    </Suspense>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }
}

const mapStateToProps = (state: IProps) => {
  return {
    collapseMenu: state.collapseMenu,
    user: state.user as IUser,
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    onToggleNavigation: () => dispatch({ type: 'COLLAPSE_MENU' }),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(windowSize(Dashboard))
