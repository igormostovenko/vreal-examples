import React, { CSSProperties } from 'react'
import { useStore } from 'stores'
import { useObserver } from 'mobx-react'
import { Icon } from 'r-components'
import styles from './styles.module.scss'
 
interface IProps {
  align: CSSProperties['textAlign']
  add: boolean
  inlineStyles: CSSProperties
}
const List: React.FC<IProps> = ({ align, add, inlineStyles }) => {
  const { builder } = useStore()
 
  return useObserver(() => (
    <>
      {builder.list &&
        builder.list.map((item, index) => (
          <li
            style={{
              border:
                builder.selectedListItem === index ? '2px dashed #000' : '2px dashed transparent',
              ...inlineStyles,
            }}
            key={index}
            onClick={() => builder.handleSelectedListItem(index)}
          >
            <input
              value={item}
              style={{ textAlign: align }}
              className={styles.input}
              onChange={({ target }) => builder.handleChangeListItem(index, target.value)}
              onFocus={() => builder.handleInputFocus(true)}
            />
          </li>
        ))}
      {add && (
        <li onClick={() => builder.handleAddListItem()} className={styles.newItem}>
          <Icon name="add" style={{ fontSize: 30, color: '#63686c' }} />
        </li>
      )}
    </>
  ))
}
 
export default List
