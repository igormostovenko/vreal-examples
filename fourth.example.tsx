import React, { Component } from 'react'
 
import { Icon } from 'rewired-components'
 
import moment from 'moment'
import classnames from 'classnames'
 
import styles from './styles.module.scss'
import Link from './Link'
import { IPosition } from 'types/Position'
import positions from 'stores/positions'
import { Button } from 'r-components'
import { notification } from 'antd'
 
interface IProps {
  item: IPosition,
  onClick: Function
  active: boolean
}
 
const primaryColor = '#0090b0'
 
const btnText = {
  publish: 'PUBLISH',
  published: 'PUBLISHED',
  unpublish: 'UNPUBLISH',
  publishFromDraft: 'PUBLISH CHANGES',
  unpublished: 'UNPUBLISHED',
}
 
class ListItem extends Component<IProps> {
  state = {
    switch: false,
    visible: false,
  }
 
  onChangeValue = (e: any) => {
    e.preventDefault()
    this.setState({ switch: !this.state.switch })
  }
 
  goToBuilder = () => {
    const { item } = this.props
    const event = new CustomEvent('builder', { detail: item })
    document.dispatchEvent(event)
  }
 
  moveToCandidates = () => {
    window.location.href = `/positions/${this.props.item.id}/submissions`
  }
 
  onPublish = (position: IPosition) => {
    if (!position.questionnaireSavedOn || !position.landings.length || !position.campaigns.length) {
      let str = `${position.questionnaireSavedOn ? '' : 'Questionnaire & '}${position.landings.length ? '' : 'Landing Pages & '}${position.campaigns.length ? '' : 'Campaigns &'}`
      str = `You do not have a ${str}`
      notification.error({ message: str.substring(0, str.length - 2) })
      return
    }
    positions.publishPosition(position.positionId, position.published)
  }
 
 
  render() {
    const { item, onClick, active } = this.props
    const time =
      item.published && item.publishedOn
        ? `${btnText.published} ${moment(item.publishedOn).fromNow()}`
        : btnText.unpublished
    return (
      <div
        className={classnames({
          [styles.item]: true,
          [styles.grayed]: item.archived,
          [styles.itemActive]: active
        })}
      >
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div className={styles.itemLeft}>
            <h4>{item.title}</h4>
            <div className={styles.time}>
              <Icon name={time === btnText.unpublished ? "control-remove" : "symbol-time"} style={{ fontSize: 13, color: '#616969' }} />
              {time}
            </div>
            <div className={classnames({
              [styles.actionsContainer]: true,
              [styles.fadein]: active
            })}>
              {!active && (
                <Link to="#" label="Edit" iconName="edit" onClick={() => onClick()} />
              )}
              {!item.archived && active && (
                <Link to={`/settings/${item.positionId}`} iconName="gear" label="Settings" />
              )}
              {active && (
                <>
                  <Link
                    active={item.questionnaireSavedOn ? true : false}
                    to={`/qustionnaire-editor/${item.positionId}`}
                    iconName="link-questionnaire"
                    label="Questionnaire"
                  />
                  <Link
                    active={item.landings.length ? true : false}
                    onClick={this.goToBuilder}
                    to="#"
                    iconName="link-landing"
                    label="Landing Pages"
                  />
                  <Link
                    active={item.campaigns.length ? true : false}
                    to={`/campaigns/${item.positionId}`}
                    iconName="link-campaigns"
                    label="Campaigns"
                  />
                </>
              )}
            </div>
          </div>
          <div className={styles.itemRight}>
            <div className={styles.statsContainer}>
              <div className={styles.stats}>
                <div>
                  <Icon
                    name="symbol-person"
                    style={{ fontSize: 28, color: '#616969', display: 'initial', marginRight: 5 }}
                  />
                  {item.todaySubmissionsCount}
                </div>
                <div>Today</div>
              </div>
              <div className={classnames(styles.stats, styles.secondaryStats)}>
                <div>{item.totalSubmissionsCount}</div>
                <div>Total</div>
              </div>
            </div>
            <Link to="#" label="Candidates" iconName="symbol-person" onClick={this.moveToCandidates} />
          </div>
        </div>
        {active && (
          <div
            className={classnames({
              [styles.row]: true,
              [styles.fadein]: active
            })}
          >
            {item.published ? (
              <Button
                containerClassName={styles.buttonPrimary}
                color={primaryColor}
                transparent
                onClick={() => positions.publishPosition(item.positionId, item.published)}
              >
                {btnText.unpublish}
              </Button>
            ) : (
                <Button
                  warning={!item.questionnaireSavedOn || !item.landings.length || !item.campaigns.length}
                  containerClassName={styles.buttonPrimary}
                  color={primaryColor}
                  transparent
                  onClick={() => this.onPublish(item)}
                >
                  {btnText.publish}
                </Button>
              )}
            {item.versionState === 'Published' && (
              <Button
                containerClassName={classnames(styles.buttonPrimary, styles.buttonSecondary)}
                color={primaryColor}
                transparent
                onClick={() => this.onPublish(item)}
              >
                {btnText.publishFromDraft}
              </Button>
            )}
 
            <Link
              to={`/settings/${item.positionId}?clone=true`}
              iconName="link-copy"
              label="Clone"
              style={{ marginLeft: 30 }}
            />
            {!item.archived && (
              <Link
                onClick={() => positions.archivePosition(item.positionId)}
                to="#"
                iconName="link-archive"
                label="Archive"
              />
            )}
          </div>
        )}
      </div>
    )
  }
}
 
export default ListItem
