import { api } from '../config'
import {
  IReport,
  IReportSolutionsTask,
  IReportTask,
  IDictionarie,
  IStudentReport,
} from 'models/report'

export async function getManagerReportSolutions(
  id: string
): Promise<IReport[]> {
  let res = await api.get(`Reports/ManagerReportSolutions?assignmentId=${id}`)
  if (res.status !== 200) throw new Error(`Error`)
  
  return res.data
}

export async function getManagerReportTasks(
  id: string
): Promise<IReportTask[]> {
  let res = await api.get(`Reports/ManagerReportTasks?assignmentId=${id}`)
  if (res.status !== 200) throw new Error(`Error`)

  return res.data
}

export async function getSolutionTasks(
  id: number
): Promise<IReportSolutionsTask[]> {
  let res = await api.get(`Reports/SolutionTasks?solutionId=${id}`)
  if (res.status !== 200) throw new Error(`Error`)

  return res.data
}

export async function getSolutionStudent(
  id: number
): Promise<IReportSolutionsTask[]> {
  let res = await api.get(`Reports/TaskScores?assignmentTaskId=${id}`)
  if (res.status !== 200) throw new Error(`Error`)
  
  return res.data
}

export async function getSolutionTaskRemark(
  id: number
): Promise<IReportSolutionsTask[]> {
  let res = await api.get(`Reports/TaskRemarks?assignmentTaskId=${id}`)
  if (res.status !== 200) throw new Error(`Error`)

  return res.data
}

export async function getStudentReport(id: string): Promise<IStudentReport> {
  let res = await api.get(`Reports/StudentReport?assignmentId=${id}`)
  if (res.status !== 200) throw new Error(`Error`)

  return res.data
}

export async function getDictionaries(): Promise<IDictionarie> {
  let res = await api.get(`Reports/Dictionaries`)
  if (res.status !== 200) throw new Error(`Error`)

  return res.data
}

export async function getSolutionInRemarks(
  assignmentId: string,
  studentId: string
): Promise<IReportSolutionsTask[]> {
  let res = await api.get(
    `Reports/SolutionInRemarks?assignmentId=${assignmentId}&studentId=${studentId}`
  )
  if (res.status !== 200) throw new Error(`Error`)

  return res.data
}

export async function getSolutionOutRemarks(
  assignmentId: string,
  studentId: string
): Promise<IReportSolutionsTask[]> {
  let res = await api.get(
    `Reports/SolutionOutRemarks?assignmentId=${assignmentId}&studentId=${studentId}`
  )
  if (res.status !== 200) throw new Error(`Error`)

  return res.data
}
